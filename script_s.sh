#! /usr/bin/env bash

# Install Docker-CE
echo "------- Install Docker-CE ----------"
sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt update
sudo apt install docker-ce -y

# Manage Docker as a non-root user
echo "----- Manage Docker as a non-root user ------"
sudo groupadd docker
sudo usermod -aG docker vagrant
newgrp docker

# Install Ansible
echo "------- Install Ansible ------"
sudo apt install ansible -y

# Install Molecule
echo "----- Install Molecule  -------"
sudo apt install python3-pip -y
sudo python3 -m pip install wheel
sudo python3 -m pip install molecule docker

# Config server
echo "-------- Config server and ansible -------"
#ssh-keygen -t rsa -f ~/.ssh/id_rsa -q -P ""
mkdir roles group_vars
sudo ansible-galaxy install -r requirements.yml